var express = require("express");
var app = express();
var bodyParser = require("body-parser");
const router = require("./routes");
const rateLimit = require("express-rate-limit");

// Enable if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
// see https://expressjs.com/en/guide/behind-proxies.html
// app.set('trust proxy', 1);

const apiLimiter = rateLimit({
  max: 1
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8000;

app.use(express.static("public")); // static assets eg css, images, js
app.use("/wa", router);
app.use("/wa", apiLimiter);

app.listen(port);
console.log("Magic happens on port " + port);
